## Challenges 16 (Instructions) – Solving Problems 

### Asynchronous Nested Callback Functions

Steps
- Clone repo or download.

Deriverables
- Upload the github/gitlab repository url to Trello.

Final result

Our client has decided that it is important to use `best practices` to solve the problem and he has given us some piece of code with `nested callback functions`. Also, our client has provided with an output example, so your work should evaluate it and reach it if you have written the functions correctly.

> You must use `best practices` and `callbacks`. 