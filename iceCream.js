// Asynchronous Nested Callback Functions

/*
The company 'IceCream Ltd' has the next steps for ice cream production:

1) Step 1 is to place the order, which takes 2 seconds. 
2) Step 2 is cut the fruit (2 seconds).
3) Step 3 is add water and ice (1 second).
4) Step 4 is to start the machine (1 second). 
5) Step 5 is to select the container (2 seconds). 
6) Step 6 is to select the toppings (3 seconds). 
7) Step 7 is to serve the ice cream which takes 2 seconds.

To establish the timing, the 'setTimeout()' function is implemented, which
also uses a callback by taking a function as an argument.


Given this implementation of Asynchronous Callbacks:


let stocks = {
  Fruits : ["strawberry", "grapes", "banana", "apple"],
  liquid : ["water", "ice"],
  holder : ["cone", "cup", "stick"],
  toppings : ["chocolate", "peanuts"],
};


let production = () =>{
    setTimeout(()=>{
        console.log("production has started")
        setTimeout(()=>{
            console.log("The fruit has been chopped")
            setTimeout(()=>{
                console.log(`${stocks.liquid[0]} and ${stocks.liquid[1]} Added`)
                setTimeout(()=>{
                    console.log("start the machine")
                    setTimeout(()=>{
                        console.log(`Ice cream placed on ${stocks.holder[1]}`)
                        setTimeout(()=>{
                            console.log(`${stocks.toppings[0]} as toppings`)
                            setTimeout(()=>{
                                console.log("serve Ice cream")
                            },2000)
                        },3000)
                    },2000)
                },1000)
            },1000)
        },2000)
    },0000)
}


let order = (fruitName, callProduction) =>{
    setTimeout(function(){
        console.log(`${stocks.Fruits[fruitName]} was selected`)
        // Order placed. Call production to start
        callProduction()
    },2000)
}

// Driver code
order(0, production);


Redefine naming each responsability in 'callback functions' in order to divide 
'ice cream production' responsabilities and also refactor the code, so finally
the following output example is reached in terminal:


```
strawberry was selected
production has started
The fruit has been chopped
water and ice Added
start the machine
Ice cream placed on cup
chocolate as toppings
serve Ice cream
```

*/


//+++ YOUR CODE GOES HERE



//production()




//order()





// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

order(0, production)
 
console.log('*~*~*~*~*~ TEST-1 *~*~*~*~*~') 


// Output Example

/*

*~*~*~*~*~ TEST-1 *~*~*~*~*~
strawberry was selected
production has started
The fruit has been chopped
water and ice Added
start the machine
Ice cream placed on cup
chocolate as toppings
serve Ice cream

*/










/*
> References:

1. Adapted from Asynchronous Javascript: https://www.freecodecamp.org/news/javascript-async-await-tutorial-learn-callbacks-promises-async-await-by-making-icecream/

*/